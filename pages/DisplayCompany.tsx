import React from 'react';
import { useRouter } from 'next/router';
import companies from '../data/companies';
import returnCompanyObject from './utils/utils';
import { Box, Text, Center } from '@mantine/core';

const DisplayCompany = () => {
  const router = useRouter();
  const { comp } = router.query;
  const company = returnCompanyObject(comp);
  console.log(company);
  return (
    <Center>
      <Box>
        <Text size="lg" weight="bold">{company[0].name}</Text>
      </Box>
    </Center>
  );
};

export default DisplayCompany;
