import React from 'react'
import { useState } from 'react';
import { Modal, Button, Group } from '@mantine/core';
import Buy from './Buy';

const BuyModal = () => {
  const [opened, setOpened] = useState(false);

  return (
    <>
    <Modal
      opened={opened}
      onClose={() => setOpened(false)}
      title="Buy Stock Here"
      size="sm"
    >
      <Buy />
    </Modal>

    <Group position="center">
      <Button onClick={() => setOpened(true)}>Buy Stock</Button>
    </Group>
  </>
  )
}

export default BuyModal;
