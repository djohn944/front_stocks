import React, { useState } from 'react';
import { NativeSelect, Paper, NumberInput, Button } from '@mantine/core';

import companies from '../../data/companies';

const Buy = () => {
  const companyList = companies.map((comps) => {
    const formattedCo = {
      label: '',
      value: '',
    };
    formattedCo.label = `${comps.name}  ${comps.todaysSharePrice}`;
    formattedCo.value = comps.name;
    return formattedCo;
  });

  const [nameValue, setNameValue] = useState(companyList[0].value);
  const [amountValue, setAmountValue] = useState(0);


  const handleClick = (e) => {
    setAmountValue(e.value);
    console.log(amountValue);
    console.log(nameValue);
  }

  return (
    <Paper padding="md" shadow="xs">
      <NativeSelect
        data={companyList}
        placeholder="Pick one"
        label="Select Stock to Buy"
        description="Transactions are final"
        required
        onChange={(event) => setNameValue(event.currentTarget.value)}
        value={nameValue}
      />
      <NumberInput
        defaultValue={18}
        placeholder="Enter Amount"
        label="Enter Amount"
        onChange={(value) => setAmountValue(value)}
        value={amountValue}
        required
      />
      <Button 
      style={{marginTop: 20}}
      onClick={handleClick}>Buy</Button>
    
    </Paper>
  );
};

export default Buy;
