import React, { useState } from 'react';
import { Modal, Button, Group } from '@mantine/core';
import { patchCompanyById, getCompanies } from '../../data/utils';
import CompaniesList from './CompaniesList';

export const NextTurn = () => {
  const [comps, setComps] = useState([]);

  const getRandomInt = (max) => {
    return Math.round(Math.floor(Math.random() * max)* 100) /100;
  }

  const testPatch = () => {
    getCompanies().then((compsFromApi) => {
      setComps(compsFromApi);
    });


comps.forEach(co => {
const id = co.company_id;
const mod = getRandomInt(90);
let setPrice = Math.round(parseFloat(co.todaysshareprice) + mod*100)/100;

console.log('price: ', co.todaysshareprice + mod);
  patchCompanyById(id, setPrice);
})
    return <CompaniesList />;
  };

  return <Button onClick={testPatch}>Test Patch</Button>;
};
