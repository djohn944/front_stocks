import React, { useEffect, useState } from 'react';
import { Table, Title, Text } from '@mantine/core';
import Link from 'next/link'
import { getCompanies } from '../../data/utils';



const CompaniesList = () => {
  const [comps, setComps] = useState([]);

  // useEffect(()=>{
  //     setComps(companies);
  // },[companies])

  // loop thro compaines array
  // create an array with title key = companies name
  // description = " Todays Share price "

  useEffect(() => {
    // if topic is not set thtn
      getCompanies().then((compsFromApi) => {
       setComps(compsFromApi);
      })

  }, [comps]);

 // console.log('comps: ',comps[0].name);

  const displayComps = comps.map((comp) => {
  
    const formattedCo = {
      Name: '',
      Price: '',
      Amount: '',
    };
    formattedCo.Name = comp.name;
    formattedCo.Price = comp.todaysshareprice;
    formattedCo.Amount = comp.amountsharesavailiable;
    return formattedCo;
  });


  const rows = displayComps.map((comp) => (
    <tr key={comp.Name}>
      <td><Link href={`/DisplayCompany?comp=${comp.Name}`}>{comp.Name}</Link></td>
      <td>{comp.Price}</td>
      <td>{comp.Amount}</td>
    </tr>
  ));
  return (
    <Table striped={true}>
      <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Amount</th>
          </tr>
      </thead>
      
      <tbody>{rows}</tbody>
    </Table>
  );
};

export default CompaniesList;
