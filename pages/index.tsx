import React from 'react';
import CompaniesList from './components/CompaniesList';
import Buy from './components/Buy';
import Header from './Header';
import BuyModal from './components/BuyModal';
import { NextTurn } from './components/NextTurn';

const Home = () => {
  return (
    <>
      <Header />
      <CompaniesList />
      <BuyModal />
      <NextTurn />
    </>
  );
};

export default Home;
