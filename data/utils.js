import axios from 'axios';

const newsApi = axios.create({
  baseURL: 'http://localhost:9090',
})

export const getCompanies = () => {
  return newsApi.get('/comps').then(({ data }) => {
    let { comps } = data;
    //console.log('from get arts: ', articles);
    return comps;
  });
};

export const patchCompanyById = (company_id, mod_price) => {
  return newsApi.patch(`/comp/${company_id}/${mod_price}`).then(({data})=>{
    let {comp} = data;
    return comp;
  })
}